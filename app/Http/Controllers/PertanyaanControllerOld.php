<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Pertanyaan;

class PertanyaanController extends Controller
{
    public function create()
    {
        return view('posts.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required|unique:pertanyaan|max:45',
            'isi' => 'required',
        ]);
        // $query = DB::table('pertanyaan')->insert([
        //     "judul" => $request["judul"],
        //     "isi" => $request["isi"]
        // ]);

        // $pertanyaan = new Pertanyaan;
        // $pertanyaan->judul = $request["judul"];
        // $pertanyaan->isi = $request["isi"];
        // $pertanyaan->save();

        $pertanyaan = Pertanyaan::create ([
            "judul" => $request['judul'],
            "isi" => $request['isi']
        ]);

        return redirect('/pertanyaan')->with('success', 'Post berhasil diajukan!');
    }

    public function index()
    {
        // $post = DB::table('pertanyaan')->get();
        $post = Pertanyaan::all();
        return view('posts.index', compact('post'));
    }

    public function show($id)
    {
        // $post = DB::table('pertanyaan')->where('id', $id)->first();
        $post = Pertanyaan::find($id);
        return view('posts.show', compact('post'));
    }

    public function edit($id)
    {
        // $post = DB::table('pertanyaan')->where('id', $id)->first();
        $post = Pertanyaan::find($id);
        return view('posts.edit', compact('post'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'judul' => 'required|max:45',
            'isi' => 'required',
        ]);

        // $query = DB::table('pertanyaan')
        //     ->where('id', $id)
        //     ->update([
        //         'judul' => $request["judul"],
        //         'isi' => $request["isi"]
        //     ]);

        $update = Pertanyaan::where('id', $id)->update([
            "judul" => $request['judul'],
            "isi" => $request['isi']
        ]);

        return redirect('/pertanyaan')->with('success', 'Post berhasil diupdate!');
    }

    public function destroy($id)
    {
        // $query = DB::table('pertanyaan')->where('id', $id)->delete();
        Pertanyaan::destroy($id);
        return redirect('/pertanyaan');
    }
}
