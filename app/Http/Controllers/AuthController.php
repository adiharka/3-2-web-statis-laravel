<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function Register () {
        return view('form');
    }

    public function Welcome (Request $r) {
        dd($r);
        return view('welcome');
    }
    
    public function Welcome_post (Request $r) {
        return view('welcome', $r);
    }
}
