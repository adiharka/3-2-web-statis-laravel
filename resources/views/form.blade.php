<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
        @csrf
        <label for="FirstName">First name:</label><br><br>
        <input type="text" name="FirstName"><br><br>
        <label for="LastName">Last name:</label><br><br>
        <input type="text" name="LastName"><br><br>
        <label for="gender">Gender:</label><br><br>
        <input type="radio" name="gender">Male<br>
        <input type="radio" name="gender">Female<br>
        <input type="radio" name="gender">Other<br><br>
        <label for="Nationality">Nationality</label><br><br>
        <select name="Nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Singaporean">Singaporean</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Australian">Australian</option>
        </select><br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="check1">
        <label for="check1">Bahasa Indonesia</label><br>
        <input type="checkbox" name="check2">
        <label for="check2">English</label><br>
        <input type="checkbox" name="check3">
        <label for="check3">Other</label><br><br>
        <label for="Bio">Bio:</label><br><br>
        <textarea name="Bio" cols="30" rows="10">
        </textarea><br><br>
        <input name="submit" type="submit" value="Sign up"><br>
    </form>
</body>
</html>