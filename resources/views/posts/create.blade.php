@extends('adminltee.master')

@section('content')

<div class="ml-3 mt-3 mb-3 mr-3 pt-3">
<h2>Tambah Data</h2>
    <form action="/pertanyaan" method="POST">
        @csrf
    <div class="form-group">
        <label for="title">Judul</label>
        <input type="text" class="form-control" name="judul" id="judul" placeholder="Masukkan judul" value="{{ old('judul','')}}">
        @error('judul')
        <div class="alert alert-danger mt-2">
            {{ $message }}
        </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">Isi</label>
        <textarea type="text" class="form-control" name="isi" id="isi" placeholder="Masukkan detail pertanyaan" rows="3">{{ old('isi','')}}</textarea>
        @error('isi')
        <div class="alert alert-danger mt-2">
            {{ $message }}
        </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>

</div>
@error('body')
<div class=”alert alert-danger”>
    {{ $message }}
</div>
@enderror
</div>

@endsection
