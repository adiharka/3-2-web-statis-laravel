@extends('adminltee.master')

@section('content')

<div class="mr-3 ml-3 mt-3">
    <h2>Edit Post {{$post->id}}</h2>
    <form action="/pertanyaan/{{$post->id}}" method="POST">
        @csrf
        @method('PUT')
        @if(session('success'))
        <div class="alert alert-success">
            {{session('success')}}
        </div>
        @endif
        <div class="form-group">
            <label for="judul">Judul</label>
            <input type="text" class="form-control" name="judul" value="{{ old('judul', $post->judul)}}" id="judul"
                placeholder="Masukkan Judul">
            @error('judul')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="isi">Isi</label>
            <input type="text" class="form-control" name="isi" value="{{ old('isi', $post->isi)}}" id="isi"
                placeholder="Masukkan Isi">
            @error('isi')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div>

@endsection
