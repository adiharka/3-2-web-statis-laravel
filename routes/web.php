<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('index');
// });
// use Illuminate\Support\Facades\Auth;
// use Illuminate\Support\Facades\Route; 
// use App\Http\Controllers\HomeController;

Route::get('/register', 'App\Http\Controllers\AuthController@Register');

Route::get('/welcome', 'App\Http\Controllers\AuthController@Welcome');
Route::post('welcome', 'App\Http\Controllers\AuthController@Welcome_post');

Route::get('/master', function() {
    return view('adminltee.master');
});

Route::get('/', function() {
    return view('adminltee.items.route');
});

Route::get('/data-tables', function() {
    return view('adminltee.items.data');
});

// Route::get('/pertanyaan', 'App\Http\Controllers\PertanyaanController@index');
// Route::get('/pertanyaan/create', 'App\Http\Controllers\PertanyaanController@create');
// Route::post('/pertanyaan', 'App\Http\Controllers\PertanyaanController@store');
// Route::get('/pertanyaan/{pertanyaan_id}', 'App\Http\Controllers\PertanyaanController@show');
// Route::get('/pertanyaan/{pertanyaan_id}/edit', 'App\Http\Controllers\PertanyaanController@edit');
// Route::put('/pertanyaan/{pertanyaan_id}', 'App\Http\Controllers\PertanyaanController@update');
// Route::delete('/pertanyaan/{pertanyaan_id}', 'App\Http\Controllers\PertanyaanController@destroy');

Route::resource('pertanyaan', 'App\Http\Controllers\PertanyaanController');